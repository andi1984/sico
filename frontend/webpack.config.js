const path = require('path');

var publicPath;

if (process.env.NODE_ENV == "development") {
    publicPath = "/static/";
} else {
    publicPath = "https://domma.gitlab.io/sico/";
}

module.exports = {
  mode: "production",
  entry: {
    admin: './src/bundles/admin.js',
    search: './src/bundles/search.js'
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(jpe?g|gif|png|svg|woff|ttf|wav|mp3)$/,
        use: 'file-loader'
      }
    ],
  },
  resolve: {
    modules: [path.resolve(__dirname, 'src'), 'node_modules'],
    extensions: [ '.tsx', '.ts', '.js' ],
  },
  output: {
    filename: 'bundle-[name]-[contenthash].js',
    path: path.resolve(__dirname, '../sico/web/static'),
    publicPath: publicPath
  },
};
