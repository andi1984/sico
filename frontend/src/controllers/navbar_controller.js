import { Controller } from "stimulus"

export default class extends Controller {
    connect() {
        let burger = this.element.getElementsByClassName("navbar-burger")[0];
        let target_id = burger.getAttribute("data-target");
        let target = document.getElementById(target_id);

        burger.addEventListener("click", () => {
            burger.classList.toggle("is-active");
            target.classList.toggle("is-active");
        });
  }
}
