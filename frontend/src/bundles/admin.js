import { init as initControllers } from "helpers/controllers";
import { init as initSentry } from "helpers/sentry";

import "bulma/css/bulma.min.css";

initSentry();
initControllers();
