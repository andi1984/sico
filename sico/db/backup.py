import json

from sqlalchemy import func
from sqlalchemy.sql import select

import sico.db.schema as s


def _write_table(con, table, order_columns, f):
    rows = con.execute(
        select([table.c.id, table.c.source, table.c.parents]).order_by(*order_columns)
    ).fetchall()
    for r in rows:
        json.dump(dict(r), f)
        f.write("\n")


def backup(con, path):
    with open(path, "w") as f:
        #   write header
        f.write("#version: 1\n")
        #   write groups
        f.write("#sico: groups\n")
        _write_table(
            con, s.groups, [func.cardinality(s.groups.c.parents), s.groups.c.id], f
        )
        #   write docs
        f.write("#sico: docs\n")
        _write_table(con, s.docs, [s.docs.c.id], f)


def _parse_items(f):
    for line in f:
        if line.startswith("#"):
            return
        yield json.loads(line)


def restore(db, path):
    if not db.empty:
        raise RuntimeError("Database is not empty.")

    with open(path) as f:
        version = f.readline().strip()
        if not version == "#version: 1":
            raise RuntimeError(f"Found unexpected version header: {version}")

        tag_ids = {}
        parents = {}
        f.readline()
        #   First insert all groups, collect mapping between old and
        #   new id and remember parents to be connected.
        for tag in _parse_items(f):
            old_id = tag["id"]
            new_id = db.groups.create(tag["source"])
            tag_ids[old_id] = new_id
            parents[new_id] = tag["parents"]
        #   Now we have all ids, so lets map old parent ids to
        #   new ids and create the connections.
        for new_id, p in parents.items():
            for old_parent in p:
                new_parent = tag_ids[old_parent]
                db.groups.use(new_id).add_parent(new_parent)

        for doc in _parse_items(f):
            new_id = db.docs.create(doc["source"])
            # for p in doc["parents"]:
            # new_parent = tag_ids[p]
            # db.docs.use(new_id).add_parent(new_parent)
