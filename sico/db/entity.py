from sqlalchemy.sql import select, and_

from sico.db.parsing import from_yaml
import sico.db.schema as s


class Entity:
    def __init__(self, connection, table, wrapper):
        self.con = connection
        self.table = table
        self.wrapper = wrapper

    def use(self, entity_id):
        return self.wrapper(entity_id, self.table, self.con)

    def create(self, source):
        return self.con.execute(
            self.table.insert().values(source=source, data=from_yaml(source))
        ).inserted_primary_key[0]

    def all(self):
        return self.con.execute(select([self.table])).fetchall()

    def filtered(self, filters):
        print(filters)
        return self.con.execute(select([self.table]).where(and_(*filters))).fetchall()

    def map_ids(self, ids):
        items = self.con.execute(
            select([self.table]).where(self.table.c.id.in_(tuple(ids)))
        ).fetchall()

        items = {i["id"]: i for i in items}
        return [items.get(i) for i in ids]

    def all_children_of(self, parent_id, limit=10):
        return list(
            self.con.execute(
                select([self.table])
                .where(self.table.c.parents.any(parent_id))
                .limit(10)
            )
        )

    def get_names(self):
        x = (
            select([self.table.c.id, self.table.c.data["name"].label("name")])
            .where(self.table.c.data.has_key("name"))
            .order_by(self.table.c.data["name"])
        )
        return self.con.execute(x).fetchall()


class Docs(Entity):
    pass


class Groups(Entity):
    def overview(self):
        return self.con.execute(s.groups_with_counts).fetchall()
