drop trigger if exists insert_parents on tags_tags;
drop trigger if exists trigger_check_tag_circle on tags_tags;
drop function if exists check_tag_circle;
drop function if exists insert_trigger_calc_parents;
drop function if exists calc_parents;
