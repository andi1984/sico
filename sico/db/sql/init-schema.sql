create table docs (
    id serial primary key,
    parents integer[] default '{}'::integer[],
    version integer,
    data jsonb
);


create table docs_versions (
    id serial primary key,
    doc_id integer,
    user_id integer,
    created timestamp,
    source text
);


create table users (
    id serial primary key,
    login varchar(100),
    password varchar(100)
);

insert into users (login, password) values ('admin', 'admin');

create table tags (
    id serial primary key,
    parents integer[] default '{}'::integer[],
    version int,
    data jsonb
);

create table tags_versions (
    id serial primary key,
    tag_id integer,
    user_id integer,
    created timestamp,
    source text
);

create table tags_docs (
    tag_id integer references tags(id) on delete cascade,
    child_id integer references docs(id) on delete cascade,
    CHECK( tag_id != child_id)
);

create table tags_tags (
    tag_id integer references tags(id) on delete cascade,
    child_id integer references tags(id) on delete cascade,
    CHECK( tag_id != child_id)
);
