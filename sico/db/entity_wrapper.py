from sqlalchemy import func
from sqlalchemy.sql import select

from sico.db.parsing import from_yaml


class EntityWrapper:
    def __init__(self, id, table, con):
        self.id = id
        self.table = table
        self.con = con

    def get(self):
        return self.con.execute(
            select([self.table]).where(self.table.c.id == self.id)
        ).fetchone()

    def update(self, source):
        self.con.execute(
            self.table.update()
            .values(source=source, data=from_yaml(source))
            .where(self.table.c.id == self.id)
        )

    def delete(self):
        self.con.execute(self.table.delete().where(self.table.c.id == self.id))

    def child_groups(self):
        if not self.id:
            where = func.cardinality(self.table.c.parents) == 0
        else:
            where = self.table.c.parents.any(self.id)
        return list(
            self.con.execute(
                select([self.table])
                .where(where)
                .order_by(self.table.c.data.op("->")("name"))
            )
        )

    def add_connection(self, parent, child):
        self.con.execute(
            self.table.update()
            .values(parents=self.table.c.parents.op("||")(parent))
            .where(self.table.c.id == child)
        )

    def remove_connection(self, parent, child):
        self.con.execute(
            self.table.update()
            .values(parents=func.array_remove(self.table.c.parents, parent))
            .where(self.table.c.id == child)
        )


class DocWrapper(EntityWrapper):
    def add_parent(self, parent):
        with self.con.begin():
            self.add_connection(parent, self.id)

    def add_child(self, child):
        if self.id == child:
            raise RuntimeError("Could not assign to itself.")

        with self.con.begin():
            self.add_connection(self.id, child)

    def remove_parent(self, parent):
        with self.con.begin():
            self.remove_connection(parent, self.id)

    def remove_child(self, child):
        with self.con.begin():
            self.remove_connection(self.id, child)


class GroupWrapper(EntityWrapper):
    def update_predecessors(self):
        self.con.execute(
            self.table.update().values(
                predecessors=func.calc_predecessors(self.table.c.id)
            )
        )

    def check_possible_circle(self, parent, child):
        predecessors = self.con.execute(
            select([self.table.c.predecessors]).where(self.table.c.id == parent)
        ).scalar()

        if child in predecessors:
            raise RuntimeError("Adding this connection would create a circle.")

    def add_parent(self, parent):
        if self.id == parent:
            raise RuntimeError("Could not assign to itself.")

        with self.con.begin():
            self.check_possible_circle(parent, self.id)
            self.add_connection(parent, self.id)
            self.update_predecessors()

    def add_child(self, child):
        if self.id == child:
            raise RuntimeError("Could not assign to itself.")

        with self.con.begin():
            self.check_possible_circle(self.id, child)
            self.add_connection(self.id, child)
            self.update_predecessors()

    def remove_parent(self, parent):
        with self.con.begin():
            self.remove_connection(parent, self.id)
            self.update_predecessors()

    def remove_child(self, child):
        with self.con.begin():
            self.remove_connection(self.id, child)
            self.update_predecessors()
