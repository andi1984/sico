import logging

from sqlalchemy import func
from sqlalchemy.sql import select, or_

from sico.db.entity import Groups, Docs
from sico.db.entity_wrapper import GroupWrapper, DocWrapper
import sico.db.schema as s
import sico.db.backup as b

LOG = logging.getLogger(__name__)


class Db:
    def __init__(self, con):
        self.con = con
        self.groups = Groups(self.con, s.groups, GroupWrapper)
        self.docs = Docs(self.con, s.docs, DocWrapper)

    @property
    def empty(self):
        groups = self.con.execute(select([func.count(s.groups.c.id)])).scalar()
        if groups != 0:
            return False
        docs = self.con.execute(select([func.count(s.docs.c.id)])).scalar()
        if docs != 0:
            return False
        return True

    def get_user(self, login, password):
        return self.con.execute(
            select([s.users]).where(
                (s.users.c.login == login) and (s.users.c.password == password)
            )
        ).fetchone()

    def get_tag_tree(self):

        tag_map = {
            t.id: dict(t.items(), _children=[])
            for t in self.con.execute(select([s.groups_with_counts])).fetchall()
        }

        top = []

        for t in tag_map.values():
            for i in t["parents"]:
                tag_map[i]["_children"].append(t)
            if not t["parents"]:
                top.append(t)

        for t in tag_map.values():
            t["_children"] = list(
                sorted(t["_children"], key=lambda t: str(t["data"]["name"].lower()))
            )

        return list(sorted(top, key=lambda t: str(t["data"]["name"])))

    #    def all_docs_in(self, group_id, limit=10):
    #        return list(
    #            self.con.execute(
    #                select([s.docs])
    #                .where(
    #                    or_(
    #                        s.docs.c.parents.any(group_id),
    #                        s.docs.c.parents.op("&&")(
    #                            select([func.array_agg(s.groups.c.id)])
    #                            .where(s.groups.c.predecessors.any(group_id))
    #                            .as_scalar()
    #                        ),
    #                    )
    #                )
    #                .limit(10),
    #            )
    #        )

    def init(self, engine):
        s.metadata.create_all(engine)
        self.con.execute(s.users.insert().values(login="admin", password="admin"))

    def reset(self, engine):
        s.metadata.drop_all(engine)
        self.init(engine)

    def backup(self, path):
        b.backup(self.con, path)

    def restore(self, path):
        b.restore(self, path)
