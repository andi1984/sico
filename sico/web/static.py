import os
import os.path
import shutil
import subprocess
import logging

LOG = logging.getLogger(__name__)
here = os.path.split(__file__)[0]
static_files = os.path.join(here, "static")


class Static:
    def __init__(self, rebuild=False):
        if rebuild:
            if os.path.exists(static_files):
                shutil.rmtree(static_files)
            subprocess.run(
                ["npm", "run", "build"], cwd=os.path.join(here, "../../frontend")
            )

            bundles = [b for b in os.listdir(static_files) if b.endswith("js")]
            with open(os.path.join(static_files, "bundles.txt"), "w") as f:
                f.write("\n".join(bundles))

        self.files = {}
        for name in os.listdir(static_files):
            with open(os.path.join(static_files, name), "rb") as f:
                self.files[name] = f.read()

    def on_get(self, req, resp, filename):
        resp.body = self.files[filename]
