import sys
import falcon

from sico.open_status import OpenStatus
from sico.web.position import Position
from sico.web.endpoint import endpoint
from sico.web.results import Jinja

from sico.db.schema import docs


def create_filter(group):
    if "filters" in group.data:
        filters = []
        for key, val in group.data["filters"].items():
            print(key, val)
            if key.lower() == "open_span":
                filters.append(docs.c.data["open_spans"][val].astext == "true")
            elif key.startswith("is_"):
                field = key[3:]
                filters.append(docs.c.data[field].astext == "true")
            else:
                raise RuntimeError(f"Invalid filter found: {key}")
        return filters
    return []


class TreePosition:
    def __init__(self, groups, default_actions=[]):
        self.actions = default_actions
        self.groups = []
        for g in groups:
            if self.groups:
                if self.groups[-1].id not in g.parents:
                    raise RuntimeError(
                        f"Invalid group hierarchy found. {self.groups[-1].id} is not in parents of {g.id}, which are {g.parents}"
                    )
            #   If we are iterating groups anyhow, we keep track of the
            #   actions. Actions on lower groups overwrite the config
            #   of higher groups.
            if "actions" in g.data:
                self.actions = g.data["actions"]

            self.groups.append(g)

    @property
    def at_top(self):
        return not self.groups

    def get_filters(self):
        filters = []
        for g in self.groups:
            filters += create_filter(g)
        return filters

    @property
    def breadcrumbs(self):
        pos = []
        for g in self.groups:
            pos.append(str(g.id))
            yield ",".join(pos), g


class Search(object):
    def on_get_root(self, req, resp):
        raise falcon.HTTPSeeOther("/search/")

    @endpoint(public=True)
    def on_get(self, db, req, resp, status):
        position = Position(encoded=status)

        groups = db.groups.map_ids(position.ids)

        tree_pos = TreePosition(groups, ["home", "phone"])

        children = db.groups.use(position.leaf).child_groups()
        children.sort(
            key=lambda d: (d.data.get("order", sys.maxsize), d.data.get("name"))
        )

        docs = []
        if not children:
            filters = tree_pos.get_filters()
            #   For now we consider it a bug if there is no filter
            #   at all, so we don't return any document.
            if filters:
                docs = db.docs.filtered(filters)
                docs.sort(
                    key=lambda d: (d.data.get("order", sys.maxsize), d.data.get("name"))
                )

        day, hour, minutes = OpenStatus.now()

        for doc in docs:
            data = doc.data
            if data:
                o = data.get("open")
                if o:
                    data["open_status"] = OpenStatus(day, hour, minutes, o)

        """Handles GET requests"""
        return Jinja(
            "search/main.jinja2",
            {
                "tree_position": tree_pos,
                "position": position,
                "children": children,
                "docs": docs,
                "active_actions": tree_pos.actions,
            },
        )
