from sico.web.endpoint import endpoint
from sico.web.results import Jinja


class StaticPages:
    @endpoint(public=True)
    def on_get_impressum(self):
        return Jinja("impressum.jinja2")

    @endpoint(public=True)
    def on_get_about(self):
        return Jinja("about.jinja2")


def setup(app):
    static = StaticPages()
    app.add_route("/about", static, suffix="about")
    app.add_route("/impressum", static, suffix="impressum")
