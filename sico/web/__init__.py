import logging
import os.path

import falcon
import bjoern
import jwt
from sqlalchemy import create_engine
import requests

from sico import config
import sico.web.docs
import sico.web.groups
import sico.web.admin
import sico.web.static
import sico.web.search
import sico.web.static_pages
from sico.web.jinja import set_bundles
from sico.db.api import Db


LOG = logging.getLogger(__name__)


class ConnectionMiddleware:
    def __init__(self, connection_string):
        LOG.info("Using connection string: {}".format(connection_string))
        self.engine = create_engine(connection_string)

    def process_request(self, req, resp):
        con = self.engine.connect()
        req.context.con = con
        req.context.txn = con.begin()
        req.context.db = Db(con)
        LOG.debug("DB connection created.")

    def process_response(self, req, resp, resource, req_succeeded):
        try:
            if req_succeeded:
                req.context.txn.commit()
                LOG.debug("Db changes commited.")
            else:
                req.context.txn.rollback()
                LOG.debug("Exception happened. Db changes rolled back.")
        finally:
            req.context.con.close()
            LOG.debug("Db connection closed.")


class AuthenticationMiddleware:
    def process_request(self, req, resp):
        LOG.debug(f"Start authentication process: {req.relative_uri}")
        # check auth via cookie
        cookies = req.get_cookie_values("sico_auth")
        if cookies:
            LOG.debug(f"Found sico_auth cookies: {cookies}")
            token = cookies[0]
            if token:
                LOG.debug(f"Found token: {token}")
                try:
                    token = jwt.decode(token, config.secret, algorithms=["HS256"])
                    user_id = token.get("user_id")
                    if user_id:
                        req.context.user_id = user_id
                        req.context.user = user_id
                        LOG.debug(
                            "Authenticated user via cookie token. User ID: {}".format(
                                user_id
                            )
                        )
                        return
                except jwt.exceptions.InvalidSignatureError as e:
                    LOG.info(f"Found invalid token. Ignoring it: {e}")
                except jwt.exceptions.DecodeError as e:
                    LOG.info(f"Found invalid token. Ignoring it: {e}")

        req.context.user_id = None
        req.context.user = None
        LOG.debug("User not authenticated.")


def handle_error(req, resp, ex, params):
    LOG.exception("Error: {}".format(str(ex)))
    raise ex


def create_app(connection):
    app = falcon.API(
        middleware=[ConnectionMiddleware(connection), AuthenticationMiddleware()]
    )

    app.add_error_handler(Exception, handle_error)

    app.req_options.auto_parse_form_urlencoded = True
    app.resp_options.secure_cookies_by_default = False

    # Resources are represented by long-lived class instances
    search = sico.web.search.Search()

    # things will handle all requests to the '/things' URL path
    app.add_route("/", search, suffix="root")
    app.add_route("/search/{status}", search)
    sico.web.static_pages.setup(app)

    sico.web.docs.setup(app)
    sico.web.groups.setup(app)
    sico.web.admin.setup(app)

    return app


def bundle_base_name(fullname):
    parts = fullname.split("-")
    if not len(parts) == 3 or not parts[0] == "bundle":
        raise ValueError(f"Invalid bundle name: {fullname}")
    return parts[1]


def serve(connection_string, dev_mode, host="0.0.0.0", port=8000):
    LOG.info("Connection string: {}".format(connection_string))
    app = create_app(connection_string)

    if dev_mode:
        LOG.info("Static files folder: {}".format(sico.web.static.static_files))
        static = sico.web.static.Static(dev_mode)
        app.add_route("/static/{filename}", static)
        with open(os.path.join(sico.web.static.static_files, "bundles.txt")) as f:
            names = [n.strip() for n in f.readlines()]
        set_bundles(
            {
                bundle_base_name(name): "/static/" + name
                for name in names
                if name.endswith("js")
            }
        )
    else:
        #   TODO: read from env variable
        bundles_root = "https://domma.gitlab.io/sico/"
        names = requests.get(bundles_root + "bundles.txt").text.split("\n")
        set_bundles(
            {
                bundle_base_name(name): bundles_root + name
                for name in names
                if name.endswith("js")
            }
        )

    LOG.info("Serving requests at {}:{}.".format(host, port))
    try:
        bjoern.run(app, "0.0.0.0", port)
    except:
        LOG.exception("Unhandled exception while trying to server app.")
        raise
