import logging

import jinja2

LOG = logging.getLogger(__name__)

env = jinja2.Environment(
    loader=jinja2.PackageLoader("sico.web", "templates"),
    autoescape=jinja2.select_autoescape(["html", "xml"]),
)


def icon_classes(item):
    #   Some items are plain SqlAlchemy rows, others are
    #   dictionaries, so we have to handle both cases for now.
    #   Should be unified in the calling code!
    if type(item) == dict:
        data = item["data"]
    else:
        data = item.data

    if data:
        icon = data.get("icon")
        if icon:
            parts = icon.split(":")
            if len(parts) == 1:
                return f"far fa-{parts[0]}"
            else:
                group, name = parts
                return f"{group} fa-{name}"


env.filters["icon_classes"] = icon_classes


def set_bundles(bundles):
    LOG.debug(f"Bundles: {bundles}")
    env.globals["bundle"] = bundles


def render(template, data):
    return env.get_template(template).render(**data)
