import logging

from sico.web.results import Jinja, Redirect, Error
from sico.web.endpoint import endpoint

LOG = logging.getLogger(__name__)


class Entities:
    def __init__(self, get_entity_domain):
        self.get_entity_domain = get_entity_domain

    @endpoint()
    def on_post(self, entity, resp, txt_body):
        try:
            entity.create(txt_body)
        except (Exception) as e:
            #        except (RuntimeError, ValueError, YAMLError, DuplicateKeyError) as e:
            return Error(str(e))
        return Redirect(f"/{self.entity_name}s")

    @endpoint()
    def on_get(self, entity, req, resp):
        return Jinja(
            f"{self.entity_name}s/overview.jinja2",
            {"items": entity.get_names(), "entity_name": self.entity_name},
        )

    @endpoint()
    def on_get_new(self, req, resp):
        return Jinja("new_item.jinja2", {"entity_type": self.entity_name})


class Entity:
    def __init__(self, get_entity_domain):
        self.get_entity_domain = get_entity_domain

    @endpoint()
    def on_get_edit(self, entity, entity_id):
        return Jinja(
            "edit_item.jinja2",
            {
                "raw_doc": entity.use(entity_id).get().source,
                "target_url": f"/{self.entity_name}/{entity_id}",
                "entity_id": entity_id,
            },
            tl=f"/{self.entity_name}/{entity_id}/edit",
        )

    @endpoint()
    def on_put(self, entity, txt_body, resp, entity_id):
        try:
            entity.use(entity_id).update(txt_body)
        except (Exception) as e:
            #        except (RuntimeError, ValueError, YAMLError, DuplicateKeyError) as e:
            return Error(str(e))
        return Redirect(f"/{self.entity_name}s")

    @endpoint()
    def on_delete(self, entity, resp, entity_id):
        entity.use(entity_id).delete()
        return Redirect(f"/{self.entity_name}s")

    @endpoint()
    def on_post_parents(self, entity, entity_id, json_body):
        tag_id = json_body["tag_id"]
        entity.use(entity_id).add_parent(tag_id)

    @endpoint()
    def on_post_children(self, entity, entity_id, json_body):
        tag_id = json_body["tag_id"]
        entity.use(entity_id).add_child(tag_id)

    @endpoint()
    def on_delete_parents(self, entity, entity_id, json_body):
        tag_id = json_body["tag_id"]
        entity.use(entity_id).remove_parent(tag_id)

    @endpoint()
    def on_delete_children(self, entity, entity_id, json_body):
        tag_id = json_body["tag_id"]
        entity.use(entity_id).remove_child(tag_id)
