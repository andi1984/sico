from inspect import signature
from functools import wraps
import json


def create_custom_injector(fkt, available_injectors):
    injectors = {}
    sig = signature(fkt)

    for name in sig.parameters:
        if name in available_injectors:
            injectors[name] = available_injectors[name]

    def injector(self, req, resp):
        return {key: get_val(self, req, resp) for key, get_val in injectors.items()}

    return injector


def create_injection_decorator(available_injectors):
    def decorator(fkt):
        injector = create_custom_injector(fkt, available_injectors)

        @wraps(fkt)
        def _fkt(self, req, resp, **kwargs):
            values = injector(self, req, resp)
            return fkt(self, **kwargs, **values)

        return _fkt

    return decorator


def inject_entity(self, req, resp):
    return self.get_entity_domain(req, resp)


def inject_req(self, req, resp):
    return req


def inject_resp(self, req, resp):
    return resp


def inject_json_body(self, req, resp):
    return json.load(req.stream)


def inject_txt_body(self, req, resp):
    return req.stream.read().decode("utf-8")


def inject_db(self, req, resp):
    return req.context.db


default_injectors = {
    "entity": inject_entity,
    "req": inject_req,
    "resp": inject_resp,
    "json_body": inject_json_body,
    "txt_body": inject_txt_body,
    "db": inject_db,
}

inject = create_injection_decorator(default_injectors)
