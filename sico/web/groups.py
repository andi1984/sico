import logging


from sico.web.entity import Entity, Entities
from sico.web.endpoint import endpoint
from sico.web.results import Jinja, Redirect


LOG = logging.getLogger(__name__)


class Groups(Entities):
    entity_name = "group"

    @endpoint()
    def on_get(self, db, entity):
        return Jinja("groups/overview.jinja2", {"tree": db.get_tag_tree()})

    @endpoint()
    def on_post_tree(self, entity, json_body):
        LOG.debug(f"Move tag: {json_body}")
        if "move" in json_body or "add" in json_body:
            target = json_body["target"]
            source = json_body["source"]

        if "add" in json_body:
            add = json_body["add"]
            if target:
                entity.use(target).add_child(add)
            return Redirect("/groups")

        if "move" in json_body:
            move = json_body["move"]
            entity.use(source).remove_child(move)
            if target:
                entity.use(target).add_child(move)
            return Redirect("/groups")


class Group(Entity):
    entity_name = "group"


def setup(app):
    def get_entity_domain(req, resp):
        return req.context.db.groups

    group = Group(get_entity_domain)
    app.add_route("/group/{entity_id:int}", group)
    app.add_route("/group/{entity_id:int}/edit", group, suffix="edit")
    app.add_route("/group/{entity_id:int}/parents", group, suffix="parents")
    app.add_route("/group/{entity_id:int}/children", group, suffix="children")

    groups = Groups(get_entity_domain)
    app.add_route("/groups", groups)
    app.add_route("/groups/new", groups, suffix="new")
    app.add_route("/groups/tree", groups, suffix="tree")
