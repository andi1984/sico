import logging

import falcon
import jwt

from sico import config

from sico.web.endpoint import endpoint
from sico.web.results import Jinja


LOG = logging.getLogger(__name__)


class Admin:
    @endpoint()
    def on_get(self, req):
        return Jinja("admin/overview.jinja2", {"user": req.context.user})

    @endpoint(public=True)
    def on_get_login(self):
        return Jinja("admin/login.jinja2")

    @endpoint(public=True)
    def on_post_login(self, db, req, resp):
        if req.content_type == "application/x-www-form-urlencoded":
            login = req.get_param("login")
            password = req.get_param("password")
            user = db.get_user(login, password)
            if user:
                LOG.debug(f"Found user: {user}")
                token = jwt.encode(
                    {"user_id": user[0]}, config.secret, algorithm="HS256"
                ).decode("utf-8")
                resp.set_cookie("sico_auth", token, path="/")
                LOG.debug(f"Login, set cookie to: {token}")
                resp.location = "/admin"
                resp.status = falcon.HTTP_301
                return
            else:
                LOG.debug(f"Ivalid user: {user}")

        resp.status = falcon.HTTP_403

    @endpoint()
    def on_post_logout(self, req, resp):
        resp.set_cookie("sico_auth", "", path="/", expires=0)
        resp.location = "/admin/login"
        resp.append_header("Turbolinks-Location", "/admin/login")
        resp.status = falcon.HTTP_301


def setup(app):
    admin = Admin()
    app.add_route("/admin", admin)
    app.add_route("/admin/login", admin, suffix="login")
    app.add_route("/admin/logout", admin, suffix="logout")
