from sico.web.entity import Entities, Entity


class Docs(Entities):
    entity_name = "doc"


class Doc(Entity):
    entity_name = "doc"


def setup(app):
    def get_entity_domain(req, resp):
        return req.context.db.docs

    doc = Doc(get_entity_domain)
    app.add_route("/doc/{entity_id:int}", doc)
    app.add_route("/doc/{entity_id:int}/edit", doc, suffix="edit")
    app.add_route("/doc/{entity_id:int}/parents", doc, suffix="parents")

    docs = Docs(get_entity_domain)
    app.add_route("/docs", docs)
    app.add_route("/docs/new", docs, suffix="new")
