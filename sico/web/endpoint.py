import logging
from functools import wraps

import falcon

from sico.web.injector import create_custom_injector, default_injectors
from sico.web.results import Result

LOG = logging.getLogger(__name__)


@falcon.before
def requires_admin(req, resp, resource, params):
    LOG.info(f"requires_admin -> user_id: {req.context.user_id}")
    if not req.context.user_id:
        raise falcon.HTTPMovedPermanently(
            "/admin/login", headers={"Turbolinks-Location": "/admin/login"}
        )
    LOG.info("Got valid admin user.")


def endpoint(public=False):
    def decorator(fkt):
        injector = create_custom_injector(fkt, default_injectors)

        @wraps(fkt)
        def _fkt(self, req, resp, **kwargs):
            values = injector(self, req, resp)
            result = fkt(self, **kwargs, **values)

            if result:
                if not isinstance(result, Result):
                    raise RuntimeError(
                        f"Result is of type {type(result)} which does not derive from Result"
                    )
                result.create_response(resp)

        if not public:
            _fkt = requires_admin(_fkt)

        return _fkt

    return decorator
