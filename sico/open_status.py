from datetime import datetime, timedelta


def is_before(hour, minutes, ref):
    return ref[0] > hour or (ref[0] == hour and minutes < ref[1])


def is_after(hour, minutes, ref):
    return hour > ref[0] or (hour == ref[0] and ref[1] < minutes)


def is_in_range(hour, minutes, start, end):
    return is_after(hour, minutes, start) and is_before(hour, minutes, end)


def is_overlapping(startA, endA, startB, endB):
    return is_in_range(*startA, startB, endB) or is_in_range(*endA, startB, endB)


valid_days = ["mo", "di", "mi", "do", "fr", "sa", "so"]

day_names = {
    "mo": "Montag",
    "di": "Dienstag",
    "mi": "Mittwoch",
    "do": "Donnerstag",
    "fr": "Freitag",
    "sa": "Samstag",
    "so": "Sonntag",
}


class OpenStatus:
    def __init__(self, day, hour, minutes, opening_hours):
        self.day = day
        self.opening_hours = opening_hours

        self.opens_at = None
        self.closes_at = None
        self.is_open = False

        if day in opening_hours:
            #   Today it is open at some time, so we might be
            #   open now. Lets see.

            for start, end in opening_hours[day]:
                if not self.opens_at and is_before(hour, minutes, end):
                    self.opens_at = (day, start)
                if is_in_range(hour, minutes, start, end):
                    self.is_open = True
                    self.closes_at = (day, end)
                    #   We have everything we need, so lets stop here
                    return
        else:
            #   Today is not open at all, so we are closed
            #   for sure.
            self.is_open = False

        if not self.opens_at:
            tmp = valid_days + valid_days
            next_days = tmp[tmp.index(day) + 1 :]

            #   When is open again? Find the first day after the
            #   current one, which is open at all. As ranges are
            #   sorted, we can just pick the first one.
            for d in next_days:
                if d in opening_hours:
                    start, _ = opening_hours[d][0]
                    self.opens_at = (d, start)
                    return

    @property
    def opens_today(self):
        return self.day == self.opens_at[0]

    @property
    def next_open_day(self):
        return day_names[self.opens_at[0]]

    def __str__(self):
        return f"OpenStatus: is_open={self.is_open}, opens_at={self.opens_at}, closes_at={self.closes_at}"

    def time_table(self):
        today, _, _ = self.now()
        #   calc a full week from today on
        tmp = valid_days + valid_days
        week = tmp[tmp.index(today) :][:7]

        for day in week:
            if day in self.opening_hours:
                yield day_names[day], self.opening_hours[day]

    @classmethod
    def now(cls):
        #   TODO: This is a hack! We have to get the time from the
        #   the client and use it here.
        now = datetime.today() + timedelta(hours=2)
        return valid_days[now.weekday()], now.hour, now.minute
