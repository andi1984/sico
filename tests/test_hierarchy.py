import pytest


def test_add_parent(db):
    group1 = db.groups.create("name: 'group1'")
    group2 = db.groups.create("name: 'group2'")

    db.groups.use(group2).add_parent(group1)


def test_no_add_to_self(db):
    group1 = db.groups.create("name: 'group1'")

    with pytest.raises(Exception):
        db.groups.use(group1).add_parent(group1)


def test_no_circles(db):
    group1 = db.groups.create("name: 'group1'")
    group2 = db.groups.create("name: 'group2'")
    group3 = db.groups.create("name: 'group3'")

    db.groups.use(group1).add_parent(group2)
    db.groups.use(group2).add_parent(group3)

    for x in db.groups.all():
        print(x)

    with pytest.raises(Exception):
        db.groups.use(group3).add_parent(group1)


def test_names(db):
    group1 = db.groups.create("name: 'group1'")
    group2 = db.groups.create("name: 'group2'")

    names = db.groups.get_names()

    assert len(names) == 2


def test_result(db):
    group1 = db.groups.create("name: 'group1'")

    x = db.groups.use(group1).get()
    assert x.data["name"] == "group1"
