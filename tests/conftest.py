"""
PyTest Fixtures.
"""

import pytest

from sico.db.api import Db


@pytest.fixture(scope="function")
def db(postgresql_db):
    con = postgresql_db.engine.connect()
    db = Db(con)
    db.init(postgresql_db.engine)
    yield db
    con.close()
