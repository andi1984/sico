import pytest

from sico.web.open_status import OpenStatus
from sico.db.parsing import normalize_opening_hours, normalize_times, normalize_days


def test_opening_hours():
    data = {"mo -MI": "12:00 -13:45", "sA": "18:00-20:00, 12:30-14:30"}
    data = [{"day": d, "time": t} for d, t in data.items()]

    expected = {
        "mo": [((12, 0), (13, 45))],
        "di": [((12, 0), (13, 45))],
        "mi": [((12, 0), (13, 45))],
        "sa": [((12, 30), (14, 30)), ((18, 0), (20, 0))],
    }

    assert normalize_opening_hours(data) == expected


def test_time_parsing():
    assert normalize_times("18:30 -\t22:00, 12:15-14:00") == [
        ((12, 15), (14, 0)),
        ((18, 30), (22, 0)),
    ]


def test_day_parsing():
    assert normalize_days("mo- MI") == ["mo", "di", "mi"]
    assert normalize_days(" dO ") == ["do"]
    assert normalize_days("fr - DO") == ["fr", "sa", "so", "mo", "di", "mi", "do"]

    with pytest.raises(ValueError):
        normalize_days("xxx")

    with pytest.raises(ValueError):
        normalize_days("do-do")

    with pytest.raises(ValueError):
        normalize_days("do - sa - so")


def test_is_open():
    opening_hours = {
        "mo": [[[12, 0], [13, 45]]],
        "di": [[[12, 0], [13, 45]]],
        "mi": [[[12, 0], [13, 45]]],
        "sa": [[[12, 30], [14, 30]], [[18, 0], [20, 0]]],
    }

    assert OpenStatus("mo", 12, 30, opening_hours).is_open

    x = OpenStatus("mo", 11, 20, opening_hours)
    assert x.is_open is False
    assert x.opens_at == ("mo", [12, 0])

    x = OpenStatus("di", 14, 00, opening_hours)
    assert x.is_open is False
    assert x.opens_at == ("mi", [12, 0])
