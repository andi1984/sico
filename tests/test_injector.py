from sico.web.injector import create_custom_injector, create_injection_decorator


def test_injector():
    class X:
        def xx(self, entity, body):
            pass

    i = create_custom_injector(X.xx, {})(None, None, None)
    assert len(i) == 0

    available = {
        "body": lambda self, req, resp: None,
        "entity": lambda self, req, resp: None,
    }

    i = create_custom_injector(X.xx, available)(None, None, None)
    assert len(i) == 2
    assert "entity" in i
    assert "body" in i


def test_create_decorator():
    available = {
        "body": lambda self, req, resp: "body",
        "entity": lambda self, req, resp: "entity",
    }

    decorator = create_injection_decorator(available)

    class X:
        @decorator
        def xx(self, body, entity):
            return body, entity

    result = X().xx(None, None)

    assert result == ("body", "entity")
